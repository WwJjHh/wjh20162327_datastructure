package List;

/**
 * Created by Administrator on 2017/10/9/009.
 */
public class Searching {
    public static int binarySearch(int[]data,int target){
        int result = 0;
        int first = 0,last = data.length-1,mid;
        while(result ==0&&first<=last){
            mid = (first+ last)/2;
            if(data[mid]==target)
                result = data[mid];
            else
            if(data[mid]>target)
                last = mid-1;
            else
                first = mid+1;
        }
        return result;
    }
    public static int linearSearch(int[]data,int target){
         int result = 0;
         int index = 0;
         while(result == 0 &&index<data.length){
             if(data[index] == target)
                 result = data[index];
             index++;
         }
         return result;
    }
    public static int Diguibinary(int[]data,int target,int first,int last){
        int result = 0;
        if(last<=first){
            int mid = (first+last)/2;
            if(data[mid]==target)
                result = data[mid];
            else if(data[mid]>target)
                return Diguibinary(data,target,first,mid-1);
            else if(data[mid]<target)
                return Diguibinary(data,target,mid+1,last);
        }
        else
            return -1;
        return result;
    }

}

