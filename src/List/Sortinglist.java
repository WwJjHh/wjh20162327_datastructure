package List;

/**
 * Created by Administrator on 2017/10/19/019.
 */
public class Sortinglist {
    public static void main(String[]args){
        int[]arr = new int[10];
        arr[0] = 90;
        arr[1] = 8;
        arr[2] = 7;
        arr[3] = 56;
        arr[4] = 123;
        arr[5] = 235;
        arr[6] = 9;
        arr[7] = 1;
        arr[8] = 653;
        Sorting.mergeSort(arr,1,10);
        Sorting.quickSort(arr,1,10);
        Sorting.bubbleSort(arr);
        Sorting.insertionSort(arr);
        Sorting.selectionSort(arr);
        for(int math:arr)
            System.out.println(math);

    }
}
