package stack;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 2017/10/9/009.
 */
public class ArrayStackTest {
    @Test
    public void peek() throws Exception {
        ArrayStack<Integer>stack = new ArrayStack<>();
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.push(6);
        int a = stack.peek();
        System.out.println(stack.toString());
        System.out.println(stack.peek().toString());//测试toString()
        System.out.println(a);
    }

    @Test
    public void isEmpty() throws Exception {
        ArrayStack<Integer>stack = new ArrayStack<>();
        assertEquals(true,stack.isEmpty());
        stack.push(2);
        stack.push(34);
        assertEquals(true,stack.isEmpty());
    }

    @Test
    public void size() throws Exception {
        ArrayStack<Integer>stack = new ArrayStack<>();
        stack.push(1);
        stack.push(34);
        assertEquals(2,stack.size());
        assertEquals(3,stack.size());
    }


}