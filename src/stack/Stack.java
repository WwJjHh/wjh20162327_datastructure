package stack;

/**
 * Created by Administrator on 2017/10/9/009.
 */
public interface Stack<T> {
    public void push (T element);
    public T pop() throws Exception, Exception;
    public T peek() throws Exception;
    public boolean isEmpty();
    public int size();
    public String toString();
}
