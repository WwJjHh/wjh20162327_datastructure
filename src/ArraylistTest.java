import org.junit.Test;
import java.util.ArrayList;
import static org.junit.Assert.*;
/**
 * Created by Administrator on 2017/10/7/007.
 */
public class ArraylistTest {
    @Test
    public void add() throws Exception {
        boolean last = false;
        ArrayList a=new ArrayList();
        last = a.add(100);
        assertEquals(true,last);
        System.out.println(a.size());      //此列表中的元素数
    }
    @Test
    public void remove() throws Exception {
        boolean last;
        ArrayList a = new ArrayList();
        a.add("wangjignhan");
        a.add("WJH");
        //a.add("tingying317");
        last = a.remove("wangjinghan");
        //assertEquals(true,last);
        last = a.remove("WJH");
        assertEquals(true, last);
    }
    @Test
    public void isEmpty() throws Exception {
        boolean last = false;
        ArrayList a=new ArrayList();
        last = a.isEmpty();
        assertEquals(true,last);
        a.add("wangjinghan");
        last = a.isEmpty();
        //  assertEquals(true,last);
    }

}