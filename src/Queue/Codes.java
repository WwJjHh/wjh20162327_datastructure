package Queue;
import Queue.CircularArrayQueue;
import java.util.ArrayDeque;
public class Codes//用电脑自带的队列
{
    public static void main ( String[] args) throws Exception {
        int[] key = {5, 12, -3, 8, -9, 4, 10};
        Integer keyValue;
        String encoded = "", decoded = "";
        String message = "All programmers are playwrights and all " + "computers are lousy actors.";
        ArrayDeque keyQueue1 = new ArrayDeque();
        ArrayDeque keyQueue2 = new ArrayDeque();
        for (int scan=0; scan < key.length; scan++)
        {
            keyQueue1.add (new Integer(key[scan]));
            keyQueue2.add (new Integer(key[scan]));
        }
        for (int scan=0; scan < message.length(); scan++)
        {
            keyValue = (Integer) keyQueue1.getFirst();
            encoded += (char) ((int)message.charAt(scan) + keyValue.intValue());
            keyQueue1.add (keyValue);
        }

        System.out.println ("Encoded Message:\n" + encoded + "\n");

        for (int scan=0; scan < encoded.length(); scan++)
        {
            keyValue = (Integer) keyQueue2.getFirst();
            decoded += (char) ((int)encoded.charAt(scan) - keyValue.intValue());
            keyQueue2.add (keyValue);
        }

        System.out.println ("Decoded Message:\n" + decoded);
    }
}