package Queue;

/**
 * Created by Administrator on 2017/10/14/014.
 */
    public interface Queue<T>
    {

        public void enqueue (T element);

        public T dequeue() throws Exception;

        public T first()throws Exception;

        public boolean isEmpty();

        public int size();

        public String toString();
    }



