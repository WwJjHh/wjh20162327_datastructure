import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.*;


/**
 * Created by Administrator on 2017/10/7/007.
 */
public class LinkedlistTest {
    @Test
    public void contains() throws Exception {
        boolean last = false;
        LinkedList a=new LinkedList();
        a.add("wangjinghan");
        a.add("WJH");
        a.add("tonying");
        last = a.contains("WJH");
        assertEquals(true,last);
        // = a.contains("WWW");
        // assertEquals(true,last);
    }

    @Test
    public void add() throws Exception {
        boolean last = false;
        LinkedList a=new LinkedList();
        last = a.add(100);
        assertEquals(true,last);
    }

    @Test
    public void remove() throws Exception {
        boolean last;
        LinkedList a=new LinkedList();
        a.add("wangjignhan");
        a.add("WJH");
        //a.add("tingying317");
        last = a.remove("wangjinghan");
        //assertEquals(true,last);
        last = a.remove("WJH");
        assertEquals(true,last);
    }

}