import org.junit.Test;

import java.util.ArrayList;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 2017/10/7/007.
 */
public class cListTest {
    @Test
    public void mergeSortedList() throws Exception {
        cList cList = new  cList();
        Random random = new Random();
        ArrayList<Integer> alist = new ArrayList<>();
        ArrayList<Integer> blist = new ArrayList<>();
        int a=0;
        for(int i=0;i<13;i++){
            a = random.nextInt(100);
            alist.add(a);
        }
        for(int i=0;i<8;i++){
            a = random.nextInt(100);
            blist.add(a);
        }
        cList.Sort(alist);
        System.out.println("alist = " + alist);
        cList.Sort(blist);
        System.out.println("blist = " + blist);
        System.out.println("cList = " +  cList.mergeSortedList(alist,blist));
    }
}