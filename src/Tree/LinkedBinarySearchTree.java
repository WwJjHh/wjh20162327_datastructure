package Tree;
import Tree.*;
public class LinkedBinarySearchTree<T extends Comparable<T>>
        extends LinkedBinaryTree<T> implements BinarySearchTree<T>
{
    public LinkedBinarySearchTree()
    {
        super();
    }
    public LinkedBinarySearchTree (T element)
    {
        root = new BSTNode<T>(element);
    }
    public void add (T item)
    {
        if (root == null)
            root = new BSTNode<T>(item);
        else
            ((BSTNode)root).add(item);
    }
    public T remove (T target) throws Exception {
        BSTNode<T> node = null;

        if (root != null)
            node = ((BSTNode)root).find(target);

        if (node == null)
            throw new Exception ("Remove operation failed. "
                    + "No such element in tree.");

        root = ((BSTNode)root).remove(target);

        return node.getElement();
    }
    public T findMin() {
        BTNode<T> node =  root;
        while (node.getLeft() != null)
            node = node.getLeft();
        return  node.getElement();
    }
    public T findMax() {
        BTNode<T> node =  root;
        while (node.getRight() != null)
            node = node.getRight();
        T Element = node.getElement();
        return  Element;
    }

}
