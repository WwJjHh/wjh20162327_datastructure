package Tree;
import Tree.*;
/**
 * Created by Administrator on 2017/10/23/023.
 */
public class LinkedBinaryTreeTest {     //参考张旭升代码后自己完成的测试
    public static void main(String[] args) {
        LinkedBinaryTree<String> current = new LinkedBinaryTree<String>("A");
        //current.root = new BTNode<String>("A");
        BTNode<String> a = current.root;
        a.left = new LinkedBinaryTree<String>("B").root;
        a.right = new LinkedBinaryTree<String>("C").root;
        (a.left).left = new LinkedBinaryTree<String>("D").root;
        (a.left).right = new LinkedBinaryTree<String>("E").root;
        (a.right).left = new LinkedBinaryTree<String>("F").root;
        (a.right).right = new LinkedBinaryTree<String>("G").root;
        System.out.println(current.isEmpty());
        try {
            System.out.println(current.contains("A"));
            System.out.println(current.contains("H"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayIterator<String>Iterator = (ArrayIterator<String>) current.preorder();
        System.out.println("先序遍历：");
        for(String i :Iterator){
            System.out.println(i);
        }
        ArrayIterator<String>Iterator2 = (ArrayIterator<String>) current.postorder();
        System.out.println("后序遍历：");
        for(String i :Iterator2){
            System.out.println(i);
        }
        System.out.println("中序遍历:" + current.toString());
    }
}
