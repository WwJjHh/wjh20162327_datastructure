package Tree;
import java.util.ArrayList;
import java.util.Scanner;
import static Tree.NewTree.*;

/**
 * Created by Administrator on 2017/10/27/027.
 */
public class NewTreeTest {
    static NewTree.node root=new NewTree.node();
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        String bp=scan.nextLine();
        String []DLR=bp.split(" ");
        for(int i=0;i<DLR.length;i++)
        {
            String qxdata=DLR[i];
            NewTree.DLR.add(String.valueOf(qxdata));
        }
        bp=scan.nextLine();
        String[]LDR=bp.split(" ");
        for(int i=0;i<LDR.length;i++)
        {
            String zxdata=LDR[i];
            NewTree.LDR.add(String.valueOf(zxdata));
        }
        BuildTree(root, NewTree.DLR, NewTree.LDR);
        System.out.println("先序遍历：");
        preorder(root);
        System.out.println();
        System.out.println("中序遍历：");
        inorder(root);
        System.out.println();
        System.out.println("后序遍历：");
        postorder(root);
        System.out.println();//ABDHIEJMNCFGKL,HDIBEMJNAFCKGL

    }
}
