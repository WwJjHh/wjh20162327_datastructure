package Tree;
import java.util.*;
/**
 * Created by Administrator on 2017/10/23/023.
 */
public class ArrayIterator<T> extends ArrayList<T> implements Iterator<T>{

    int iteratorModCount;
    int current;
    public ArrayIterator()
    {
        iteratorModCount = modCount;
        current = 0;
    }
    public boolean hasNext() throws ConcurrentModificationException
    {
        return super.iterator().hasNext();
    }
    public T next() throws ConcurrentModificationException
    {
        return super.iterator().next();
    }


    public void remove() throws UnsupportedOperationException
    {
        throw new UnsupportedOperationException();
    }
}


