package Tu.Linjiejuzhen;

import java.util.ArrayList;

import static Tu.Linjiejuzhen.Linjie.DeepTraGraph;

/**
 * Created by Administrator on 2017/11/20/020.
 */
public class Test {
    public static void main(String[] args) {

        int n=5; //5个结点
        int e=5; //5条边

        Linjie g = new Linjie(n);
        Object[] vertices ={"A","B","C","D","E"};
        Bian[] weights = new Bian[]{new Bian(0,1,10),new Bian(0,4,20),
                new Bian(2,1,40), new Bian(1,3,30),new Bian(3,2,50)};
        try
        {
            Bian.createAdjGraphic(g, vertices, n, weights, e);
            System.out.println("构造的邻接矩阵如下：");
            g.ToString();
           // DeepTraGraph(g);
            System.out.println("结点的个数："+g.getNumOfVertice());
            System.out.println("边的个数："+g.getNumEdges());
            g.removeEdges(0, 4);
            System.out.println("删除一条边之后：");
            //g.ToString();
            System.out.println("结点的个数："+g.getNumOfVertice());
            System.out.println("边的个数："+g.getNumEdges());
            g.insertEdges(0,4,34);
            System.out.println("添加一条边之后：");
            System.out.println("结点的个数："+g.getNumOfVertice());
            System.out.println("边的个数："+g.getNumEdges());
            g.insertVertice("F");
            System.out.println("添加一个结点之后：");
            System.out.println("结点的个数："+g.getNumOfVertice());
            System.out.println("边的个数："+g.getNumEdges());
            g.removeVertice("A");
            System.out.println("删除一个结点之后：");
            System.out.println("结点的个数："+g.getNumOfVertice());
            ArrayList<Object> arr = g.iteratorBFS(0);
            System.out.print("广度遍历：");
            for(Object i:arr)
                System.out.print(i+" ");
        }
        catch(Exception ex)
        {

        }
    }
}
