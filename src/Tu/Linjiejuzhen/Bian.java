package Tu.Linjiejuzhen;

/**
 * Created by Administrator on 2017/11/20/020.
 */
public class Bian {
    int start;  //起点
    int end;  //终点
    int weight; //权值

    Bian(int start,int end,int weight)
    {
        this.start = start;
        this.end = end;
        this.weight = weight;
    }

    public static void createAdjGraphic(Linjie g, Object[] vertices, int n,Bian[] weight,int e)
            throws Exception
    {
        //初始化结点
        for(int i=0;i<n;i++)
        {
            g.insertVertice(vertices[i]);//插入节点
        }
        //初始化所有的边
        for(int i=0;i<e;i++)
        {
            g.insertEdges(weight[i].start, weight[i].end, weight[i].weight);
        }
    }
}
