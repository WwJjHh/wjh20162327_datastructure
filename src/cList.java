import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/10/7/007.
 */
public class cList {
    ArrayList clist;
    public  List<? extends Comparable> mergeSortedList(List<? extends Comparable> aList,
                                                       List<? extends Comparable> bList){
        //public static List<？ extends Comparable> mergeSortedList(List<？ extends Comparable> aList,
        //  List<？ extends Comparable> bList)
        clist = new ArrayList();
        for(Comparable i:aList)
           // for(Comparable i=0;i.compareTo(aList.size());i++) {
                clist.add(i);
            //}
        for (Comparable i:bList)
            //for(Comparable i=0;i<bList.length;i++)
            clist.add(i);
        Sort(clist);
        return clist;
    }
    /*public void Sort(List<? extends Comparable> list){
        int position,scan;
        for(position = list.size();position>=0;position--){
            for(scan = 0;scan<=position - 1;scan++)
                if(list.get(scan).compareTo(list.get(scan+1)) < 0)
                    //swap(cList,scan,scan+1);
                    swap((List<Comparable>) list,scan,scan+1);
        }
    }*/
    public void Sort(List<? extends Comparable> list){
        int min;

        for (int index = 0; index < list.size()-1; index++)
        {
            min = index;
            for (int scan = index+1; scan < list.size(); scan++)
                if (list.get(scan).compareTo(list.get(min)) < 0)
                    min = scan;
            swap((List<Comparable>) list, min, index);
        }
    }
    private static void swap ( List<Comparable> list, int index1, int index2)
    {
        Comparable temp = list.get(index1);
        Comparable A =  list.get(index2);
        list.set(index1,A);
        list.set(index2,temp);
    }
    public String toString() {
        return "cList{" + "list=" + clist + '}';
    }
}
